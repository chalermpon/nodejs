//http://node.green/
// Test Array
const map = {
  21:'Chalermpon Dasri',
  22:'Kitsada Makthrapsukho',
  23:'Witsanu Sangjong',
  24:'Trintanut Charoenpool',
  25:'Pornprukra Boonsong',
  26:'Nattaphong Matha'
}

// definition with Promise
//1. get name form id first
const getNameById = (id)=>{
  return new Promise((onResolve,onReject)=>{
    if(map[id]!=undefined) onResolve(map[id])
    else onReject(`Cannot find user id ${id}`)
  })

  // var promisHandler = function(r,j){
  //   var result = map[id]
  //   if(result==undefined){
  //     j('Error finding this id')
  //   } else {
  //     r(result)
  //   }
  //
  //
  // }
  // var promObj = new Promise(promisHandler)
  // return promObj

}


//2. show result name
const showName = (name) =>{
  console.log(`Hello ${name}`)
}

// basic promise call
// getNameById(27)
// .then(showName)
// .catch((err)=>{
//   console.error(err);
// })

// async function call
async function showByStep(id){
  try{
    const name = await getNameById(id)
    console.log('test')
    await showName(name)

  } catch(error) {
    console.error(error)
  }
}
showByStep(21);
