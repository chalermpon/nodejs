const router = require('express').Router()

// Root route for api
router.get('/',(req,res,next)=>{
    res.end('you call /')
})

// ont the way to hell
router.get('/test', (req, res, next)=> {
    const dbc= req.app.get('dbconnection')
    console.log('test 1')
    dbc.query('SELECT * FROM messages;',(err,results,f)=>{
      if(!err){
        console.log('test 2')
        res.json(results)
      } else {
        console.log('test 3')
        res.json(err)
      }
    })
    console.log('test 4')
})
// promise + async wait
router.get('/test2',(req,res,next)=>{
  // promise fn define
  const dbquery = ()=>{
    let dbc = req.app.get('dbconnection')
    return new Promise((resolve,reject)=>{
      dbc.query('SELECT * FROM messages;',(err,results,f)=>{
        if(!err) resolve(results)
         else reject(err)
      })
    })
  }

  //define async fn
  async function tryAsync(){
      try{
        console.log('test 1')
        const r = await dbquery()
        console.log('test 2')
        res.json(r)
      }catch(err){
        console.log('test 3')
        res.json(err)
      }
      console.log('test 4')
  }

  //invoke fn
  tryAsync();


})



// export module
module.exports = router;
