const mysql = require('mysql')
const express = require('express')
const bodyParser = require('body-parser')


const app = express()
const router = express.Router()

app.use(bodyParser.urlencoded({extended: true}))
app.use(bodyParser.json())
app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});
const port = 8080
const dbparams = {
  host:'103.253.73.104',
  user:'root',
  port:9999,
  password:'x3b7ky',
  database:'test'
}
const dbconnection = mysql.createConnection(dbparams)
// for further use
app.set('dbconnection',dbconnection);

// default route
router.all('*', (req, res, next)=> {
    // default all -- authentication
    console.log('default')

    next(); // advance to next route
})


// Route registration
app.use(router) // authentication route
app.use(require('./routes/test')) // v1 api

// Starting server...
app.listen(port)
console.log('Application running at port: ' + port)
